use Test::Most tests => 1;

use strictures 2;
use App::Parcimonie;

ok(gpg_is_v21(), "parcimonie and its test suite require GnuPG 2.1 or newer.");
