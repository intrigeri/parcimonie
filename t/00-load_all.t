use Test::Most;

use strictures 2;
use Path::Tiny;
use Module::Pluggable::Object;
eval { require Win32; };

# progs
# my @progs=path("bin", "parcimonie");
# foreach my $file (@progs) {
#         ok(system("perl -c $file") eq 0, $file);
# }

# libs
my $finder = Module::Pluggable::Object->new(
               search_path => [ 'App::Parcimonie' ],
             );
foreach my $class (grep !/\.ToDo/,
                   sort do { local @INC = ('lib'); $finder->plugins }) {
  use_ok($class);
}

done_testing();
