package Test::Utils;

use strictures 2;

use IPC::System::Simple qw{system systemx};
use Path::Tiny;

my $gnupg_pubkeys_dir = path('t', 'data', 'pubkeys');

sub new_temporary_GnuPG_homedir {
    my $gnupg_homedir = Path::Tiny->tempdir();
    system("cp -a t/data/gnupg_homedir/*.conf '$gnupg_homedir'");
    system("gpg --homedir '$gnupg_homedir' --quiet --import $gnupg_pubkeys_dir/*.asc");
    systemx('gpg', '--homedir', $gnupg_homedir, '--quiet', '--update-trustdb');
    return $gnupg_homedir;
}

sub cleanup_temporary_GnuPG_homedir {
    my $gnupg_homedir = shift;
    system("GNUPGHOME='$gnupg_homedir' gpgconf --kill all 2>/dev/null || true");
    # $gnupg_homedir itself will be cleaned up by File::Temp
}

1;
