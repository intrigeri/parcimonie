#!perl

use strictures 2;

BEGIN {
  unless ($ENV{RELEASE_TESTING}) {
    require Test::More;
    Test::More::plan(skip_all => 'these tests are for release candidate testing');
  }
}

use LWP::Online ':skip_all';
use Test::Most tests => 2;
use lib qw{t/lib};
use Test::Utils;

use App::Parcimonie;

my $nonexistent_keyid = 'A'x40;
my $existent_keyid    = '6F818B215E159EF3FA26B0BE624DC565135EA668';
my $gnupg_homedir     = Test::Utils::new_temporary_GnuPG_homedir();
my $gnupg_options     = { homedir => $gnupg_homedir->stringify };

$ENV{LC_ALL} = 'C';

dies_ok { gpgRecvKeys([ $nonexistent_keyid ], $gnupg_options) }
    "gpgRecvKeys throws an exception when trying to receive a non-existing key.";

like(
    gpgRecvKeys([ $existent_keyid ], $gnupg_options),
    '/gpg: \s+ Total\snumber\sprocessed: \s+ 1\n
gpg: \s+ (?:unchanged|new\s+signatures) : \s+ /xms',
    "gpgRecvKeys returns an expected string when receiving an existing key."
);

Test::Utils::cleanup_temporary_GnuPG_homedir($gnupg_homedir);
