#!perl

use Test::Most tests => 1;
use strictures 2;
use lib qw{t/lib};
use Test::Utils;

my $expected_pubkeys = 2;

$ENV{LC_ALL} = 'C';

my $gnupg_homedir = Test::Utils::new_temporary_GnuPG_homedir();

use App::Parcimonie;
is(scalar gpgPublicKeys({ homedir => $gnupg_homedir->stringify }),
   $expected_pubkeys,
   "There are $expected_pubkeys public keys in the keyring."
);

Test::Utils::cleanup_temporary_GnuPG_homedir($gnupg_homedir);
