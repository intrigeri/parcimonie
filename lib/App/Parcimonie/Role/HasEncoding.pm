=head1 NAME

App::Parcimonie::Role::HasEncoding - role to provide an Encode::Encoding objet for the codeset being used

=head1 SYNOPSIS

    use Moo;
    with 'App::Parcimonie::Role::HasEncoding';
    sub foo { $self->encoding->decode("bla") }

    See App::Parcimonie::Daemon for a real-life usage example.

=cut

package App::Parcimonie::Role::HasEncoding;

use Encode qw{find_encoding};

use Moo::Role; # Moo::Role exports all methods declared after it's "use"'d
use MooX::late;

with 'App::Parcimonie::Role::HasCodeset';

use namespace::clean;

has 'encoding' => (
    isa        => 'Encode::Encoding|Encode::XS',
    is         => 'ro',
    lazy_build => 1,
);

sub _build_encoding {
    my $self = shift;
    find_encoding($self->codeset);
}

no Moo::Role;
1; # End of App::Parcimonie::Role::HasEncoding
