=head1 NAME

App::Parcimonie::DBus::Object - provide parcimonie's D-Bus service and interface

=cut

package App::Parcimonie::DBus::Object;

use strict;
use warnings;

# We're going to be a DBus object
use base qw{Net::DBus::Object};

# Specify the main interface provided by our object
use Net::DBus::Exporter qw{org.parcimonie.daemon};

use namespace::clean;

sub new {
    my $class = shift;
    my $service = shift;
    my $self = $class->SUPER::new($service, "/org/parcimonie/daemon/object");
    bless $self, $class;
    return $self;
}

# args: keyid
dbus_signal("FetchBegin", ["string"]);

# args: keyid, success, error message
dbus_signal("FetchEnd", ["string", "bool", "string"]);

# args: duration
dbus_signal("Sleeping", ["uint32"]);
