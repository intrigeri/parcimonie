User experience design
======================

The historical `parcimonie-applet` was designed based on the assumption that
_every_ operation performed by `parcimonie` was interesting to the user, and
should therefore be reported to them, both:

 - in real time (icon changing while fetching a key);
 - via a detailed log reachable via the _View log_ item in the applet's
   context menu.

Almost 10 years after I made this design decision, I have come to the conclusion
that it was flawed. More specifically, as a user:

 - I don't want to be distracted by that information.

   I suspect that to most users, most of the information provided by
   `parcimonie-applet` was non-actionable noise.

 - I could be interested in being alerted in case parcimonie fails to do the job
   it took responsibility for.

   For example, if the key refresh failure rate is unexpectedly high, which
   might indicate there is a bug or a configuration problem.

Nowadays, before designing user interactions, I would get help from someone more
skilled than me at UX design.

If anyone wants to write some sort of user interface for parcimonie:

 - The rest of this document provides hopefully useful information
   and pointers.

 - I'm open to discussing changes to the `parcimonie`'s D-Bus interface,
   that would be needed implement user interfaces.

-- intrigeri

Historical background
=====================

parcimonie used to include a user interface, in the form of a system tray applet
called `parcimonie-applet`, which displayed an icon in the "system tray" or
"notification area", following the FreeDesktop System Tray Specification on X11.

The underlying technologies have been deprecated in GNOME 3 and GTK+ 3.14.
In July 2018, the author of this applet documented that:

 - He could not support that applet anymore.
 - That applet was deprecated and would be removed from the parcimonie
   releases at some point, unless someone took over its maintenance.

Eventually, `parcimonie-applet` was removed in parcimonie 0.12.0, in April 2020.

Past discussions
================

Some discussions happened already about how a user interface for parcimonie
could be implemented. These discussions were mostly focused on adapting the
historical `parcimonie-applet` code and/or UX to the evolution of underlying
desktop technologies:

 - https://bugs.debian.org/799942
 - https://bugs.debian.org/850217

For example, people suggested using the FreeDesktop Status Notifier
Item specification.

How to get status information about parcimonie's activities
===========================================================

D-Bus
-----

When it's passed the `--with-dbus` command line option, `parcimonie` registers
the `org.parcimonie.daemon` D-Bus service. Then `parcimonie` sends updates there
about its activities, via D-Bus signals:

 - When `parcimonie` starts sleeping, parcimonie it the `Sleeping` signal.
 
   Extra data passed to the signal:

    - the number of seconds parcimonie will sleep

 - When `parcimonie` starts attempting to refresh a key, it sends the
   `FetchBegin` signal.
 
   Extra data passed to the signal:

    - the ID of the key to refresh
 
 - When `parcimonie` has completed a key refresh attempt operation,
   it sends the `FetchEnd` signal.

   Extra data passed to the signal:

    - the ID of the key `parcimonie` tried to refresh
    - whether the key was successfully refreshed: 1 (success) or 0 (failure)
    - the error output from GnuPG, if any

JSON on standard output
-----------------------

When it's not passed the `--with-dbus` command line option, `parcimonie`
reports the exact same information it would otherwise send as D-Bus signals:

 - encoded in JSON
 - to STDOUT
